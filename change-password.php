<?php
include ('./classes/DB.php');
include ('./classes/Login.php');
    $tokenIsValid = False;
if (Login::isLoggedIn())
{
  if (isset($_POST['changepassword']))
  {
    $oldpassword = $_POST['oldpassword'];
    $newpassword = $_POST['newpassword'];
    $newpasswordrepeat = $_POST['newpasswordrepeat'];
    $userid = Login::isLoggedIn();
    if (password_verify($oldpassword, DB::query('SELECT password FROM users WHERE id=:userid', array(':userid'=>$userid))[0]['password']))
    {
      if ($newpassword == $newpasswordrepeat)
      {
          if (strlen($newpassword) >= 6 && strlen($newpassword) <= 60)
          {
            DB::query('UPDATE users SET password=:newpassword WHERE id=:userid', array(':newpassword' =>password_hash($newpassword, PASSWORD_BCRYPT), 'userid'=>$userid));
            echo 'password changed Success!';
            DB::query('Delete FROM password_tokens WHERE user_id=:user_id', array(':user_id'=>$user_id));
          }
      }
      else {
        echo 'password dont match';
      }
    }
    else {
      echo 'Incorrect oldpassword';
    }
  }
}
  else {

    if (isset($_GET['token'])){
    $token = $_GET['token'];
    if (DB::query('SELECT user_id FROM password_tokens WHERE token=:token', array(':token'=>sha1($token))))
    {
          $user_id = DB::query('SELECT user_id FROM password_tokens WHERE token=:token', array(':token'=>sha1($token)))[0]['user_id'];
          $tokenIsValid = True;
          if (isset($_POST['changepassword']))
          {
            $newpassword = $_POST['newpassword'];
            $newpasswordrepeat = $_POST['newpasswordrepeat'];
            {
              if ($newpassword == $newpasswordrepeat)
              {
                  if (strlen($newpassword) >= 6 && strlen($newpassword) <= 60)
                  {
                    DB::query('UPDATE users SET password=:newpassword WHERE id=:userid', array(':newpassword' =>password_hash($newpassword, PASSWORD_BCRYPT), 'userid'=>$userid));
                    echo 'password changed Success!';
                  }
              }
              else {
                echo 'password dont match';
              }
            }
          }

    }
    else {
      die('Token Invalid');
    }}else {
    die('Not logged in');
}}
?>

<h1>Change your password</h1>
<form action="<?php if (!$tokenIsValid) { echo 'change-password.php'; } else { echo 'change-password.php?token='.$token.''; } ?>" method="post">
<?php if (!$tokenIsValid) { echo '<input type="password" name="oldpassword" value="" placeholder="Current Password ..."><p />'; } ?>
  <input type="password" name="newpassword" value="" placeholder="New password"><p />
  <input type="password" name="newpasswordrepeat" value="" placeholder="Repeat password"><p />
  <input type="submit" name="changepassword" value="Change Password">
</form>
